import os
import glob
import random
import cv2
import numpy as np
import math
import xml.etree.ElementTree as ET

for x in range(500):
    random_background_file = random.choice(glob.glob('images/background/*.jpg'))
    
    background_image = cv2.imread(random_background_file)
    
    if background_image.shape[1] > background_image.shape[0]:
        dim = (1024, int(background_image.shape[0] * 1024 / background_image.shape[1]))
        generated_image = cv2.resize(background_image, dim, interpolation=cv2.INTER_AREA)    
    else:
        dim = (int(background_image.shape[1] * 1024 / background_image.shape[0]), 1024)
        generated_image = cv2.resize(background_image, dim, interpolation=cv2.INTER_AREA)    
    
    new_filename = '/Users/mbolaris/git/coin_detection_data_generator/generated_dataset/' + 'generated' + str(x) 
    
    # create XML 
    output_root = ET.Element('annotation')
    folder = ET.SubElement(output_root, 'folder')
    folder.text = 'dataset'
    child = ET.SubElement(output_root, 'filename')
    child.text = 'generated' + str(x) + '.jpg'
    path = ET.SubElement(output_root, 'path')
    path.text = new_filename + '.jpg'
    
    source = ET.SubElement(output_root, 'source')   
    database = ET.SubElement(source, 'database')
    database.text = 'Unknown'
    
    size = ET.SubElement(output_root, 'size')   
    width = ET.SubElement(size, 'width')
    width.text = str(generated_image.shape[1]) 
     
    height = ET.SubElement(size, 'height')
    height.text = str(generated_image.shape[0])
    
    depth = ET.SubElement(size, 'depth')
    depth.text = str(generated_image.shape[2])
    
    segmented = ET.SubElement(output_root, 'segmented')
    segmented.text = '0'
    
    taken_spots = []
    
    num_coins = random.randint(2,8)
    
    for i in range(num_coins):
        random_coin_file = random.choice(glob.glob('/Users/mbolaris/git/coin_scraper/dataset/*.xml'))
        print(random_coin_file)
        coin_image = cv2.imread(random_coin_file[:-4]+'.jpg')
        input_tree = ET.parse(random_coin_file)
        input_root = input_tree.getroot()
        input_coin_object = random.choice(input_root.findall('object'))

        output_coin_object = ET.SubElement(output_root, 'object') 
        name = ET.SubElement(output_coin_object, 'name')
        name.text = input_coin_object[0].text
        name = ET.SubElement(output_coin_object, 'pose')
        name.text = input_coin_object[1].text
        
        truncated = ET.SubElement(output_coin_object, 'truncated')
        truncated.text = '0'
        difficult = ET.SubElement(output_coin_object, 'difficult')
        difficult.text = '0'
        
        input_xmin = int(input_coin_object[4][0].text)
        input_ymin = int(input_coin_object[4][1].text)
        input_xmax = int(input_coin_object[4][2].text)
        input_ymax = int(input_coin_object[4][3].text)
        
        coin_image = coin_image[input_ymin:input_ymax, input_xmin:input_xmax]

        r = 200 / coin_image.shape[1]
        dim = (200, int(coin_image.shape[0] * r))
        coin_image = cv2.resize(coin_image, dim, interpolation=cv2.INTER_AREA)
        
        rotation_angle = random.randint(0,360)
        
        rows = coin_image.shape[0]
        cols = coin_image.shape[1]
        
        M = cv2.getRotationMatrix2D((rows/2, cols/2), rotation_angle, 1)
        coin_image = cv2.warpAffine(coin_image, M, (rows,cols))
        
        coin_image[np.where((coin_image == [0,0,0]).all(axis = 2))] = [255,255,255]
        
        # Now create a mask of logo and create its inverse mask also
        img2gray = cv2.cvtColor(coin_image, cv2.COLOR_BGR2GRAY)
        img2gray = cv2.bitwise_not(img2gray)
        ret, mask = cv2.threshold(img2gray, 15, 255, cv2.THRESH_BINARY)
        mask_inv = cv2.bitwise_not(mask)
        
        spot_found = False

        while not spot_found:
            y = random.randint(0, generated_image.shape[0]-coin_image.shape[0])
            x = random.randint(0, generated_image.shape[1]-coin_image.shape[1])
            too_close = False
            for spot in taken_spots:
                distance = math.sqrt((spot[0] - x)**2 + (spot[1] - y)**2)
                print(distance)
                if distance < coin_image.shape[0]:
                    too_close = True
                    break
            if not too_close:    
                taken_spots.append((x, y))    
                spot_found = True    
    
        bndbox = ET.SubElement(output_coin_object, 'bndbox')
        xmin = ET.SubElement(bndbox, 'xmin')
        xmin.text = str(x)
        ymin = ET.SubElement(bndbox, 'ymin')
        ymin.text = str(y)
        xmax = ET.SubElement(bndbox, 'xmax')
        xmax.text = str(x+coin_image.shape[1])
        ymax = ET.SubElement(bndbox, 'ymax')
        ymax.text = str(y+coin_image.shape[0])
        
        roi = generated_image[y:y+coin_image.shape[0], x:x+coin_image.shape[1]]
        # Now black-out the area of the coin
        coin_bg = cv2.bitwise_and(roi, roi, mask = mask_inv)

        # Take only region of logo from logo image.
        new_image_fg = cv2.bitwise_and(coin_image, coin_image, mask = mask)

        # Put logo in ROI and modify the main image
        dst = cv2.add(coin_bg, new_image_fg)
        generated_image[y:y+coin_image.shape[0], x:x+coin_image.shape[1]] = dst
    
    output_tree = ET.ElementTree(output_root)
    output_tree.write(new_filename + '.xml')
    cv2.imwrite(new_filename + '.jpg', generated_image)